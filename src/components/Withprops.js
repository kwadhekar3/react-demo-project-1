/* With Class Componaent using Props Methods
import React, {Component} from "react";

class Withprops extends Component {
    render() {
    return <h1>Sanvi {this.props.name}</h1>;
    }
}
export default Withprops;
*/
/* With Functional  Componaent using Props Methods
import React from "react";

const Withprops = () =>{
    return <h1>Swati Kishor Wadhekar</h1>
};
export default Withprops;
*/

import React from "react";

const Withprops = (props) => {
return <h1>Hello {props.name}</h1>
};

export default Withprops;