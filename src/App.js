import React from "react";

import Withprops from "./components/Withprops";

const App = () => {
  return(
    <div>
      <Withprops name="Kishor"/>
      <Withprops name="Sanvi"/>
      <Withprops name="Swati"/>
    </div>
  );
};
export default App;