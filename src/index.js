import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
//import Withprops from "./components/Withprops";
/* Without Props Methods 
ReactDOM.render(<Withprops />, document.getElementById("root"));
*/
/* With Props Methods Use
ReactDOM.render(<Withprops name="Swati Wadhekar"/>, document.getElementById("root"));
*/

ReactDOM.render(<App />, document.getElementById("root"));